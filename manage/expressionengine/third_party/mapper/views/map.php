!window.google && document.write('<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"><\/script>');

jQuery(function($)
{
	if(typeof mapper_build_canvas == 'undefined')
	{
		var mapper_build_canvas = function(id, address, width, height, zoom, description)
		{
			var canvas = $('div#gmap_' + id);
			canvas.css('width', width);
			canvas.css('height', height);

			var geocoder = new google.maps.Geocoder();
			geocoder.geocode(
			{
			   address : address
			}, function(results, status)
			{
				if ( status == google.maps.GeocoderStatus.OK)
				{
					var latlng = results[0].geometry.location;
					var map = new google.maps.Map(canvas.get(0),
					{
					   zoom : zoom,
					   center : latlng,
					   mapTypeId : google.maps.MapTypeId.ROADMAP,
					   mapTypeControl : true,
					   mapTypeControlOptions :
					   {
						   style : google.maps.MapTypeControlStyle.DROPDOWN_MENU
					   },
					   navigationControl : true,
					   navigationControlOptions :
					   {
						   style : google.maps.NavigationControlStyle.SMALL
					   }
					});

					var marker = new google.maps.Marker(
					{
					   map : map,
					   position : latlng
					});

					if (description != '')
					{
						var info = new google.maps.InfoWindow(
						{
						   content : description
						});
						google.maps.event.addListener(marker, 'click', function()
						{
							info.open(map, marker);
						});

					}
				}
			});
		}
	}

	mapper_build_canvas('<?php echo $canvas_id; ?>', "<?php echo $params['address']; ?>", "<?php echo $params['width']; ?>", "<?php echo $params['height']; ?>", <?php echo $params['zoom']; ?>, "<?php echo $params['description']; ?>");
});
